#!/bin/bash

## Telegraf config
config_file="/etc/telegraf/telegraf.conf"

## VMWare guest env
output_url=$(vmtoolsd --cmd "info-get guestinfo.ovfEnv" | grep "telegraf_output_url" | cut -d\" -f4)
output_api_key=$(vmtoolsd --cmd "info-get guestinfo.ovfEnv" | grep "telegraf_api_key" | cut -d\" -f4)
hostname=$(vmtoolsd --cmd "info-get guestinfo.ovfEnv" | grep "hostname" | cut -d\" -f4)
ipaddr=$(vmtoolsd --cmd "info-get guestinfo.ovfEnv" | grep "ipaddr" | cut -d\" -f4)
netmask=$(vmtoolsd --cmd "info-get guestinfo.ovfEnv" | grep "netmask" | cut -d\" -f4)
gateway=$(vmtoolsd --cmd "info-get guestinfo.ovfEnv" | grep "gateway" | cut -d\" -f4)
dns=$(vmtoolsd --cmd "info-get guestinfo.ovfEnv" | grep "dns" | cut -d\" -f4)

## default nic
default_nic="eth0"

## Setup network
if [ -n "${hostname}" ] && [ -n "${default_nic}" ] && [ -n "${ipaddr}" ] && [ -n "${netmask}" ] && [ -n "${gateway}" ] && [ -n "${dns}" ]; then

echo ${hostname} > /etc/hostname
sed -i "s/codenotary/${hostname}/" /etc/hosts
hostnamectl set-hostname ${hostname}

cat > /etc/network/interfaces <<EOL
auto lo
iface lo inet loopback

auto ${default_nic}
iface ${default_nic} inet static
    address ${ipaddr}
    netmask ${netmask}
    gateway ${gateway}
    dns-nameservers ${dns}
EOL
else
cat > /etc/network/interfaces <<EOL
auto lo
iface lo inet loopback

auto ${default_nic}
iface ${default_nic} inet dhcp
EOL
fi

## Configure Telegraf
if [ -f "/etc/telegraf/telegraf.conf" ] && [ -n "${output_url}" ] && [ -n "${output_api_key}" ]; then
    sed -i.bak -e "s#url = \"\"#url = \"${output_url}\"#" -e "s#X-API-KEY = \"\"#X-API-KEY = \"${output_api_key}\"#" ${config_file}
else
    echo "Error: Telegraf config fail"
    exit 21
fi
