# Codenotary Interview

## Goal
```
Create a Linux virtual appliance that can be used to deploy a Linux system that ships metrics to cloud.opvizor.com after start:

Create a Debian/or Photon virtual appliance (ova) that can be deployed on a VMware system (Workstation or ESXi)
While importing there should be a size choice for the VM (small (2vCPU, 2GB, 20GB disk) , medium (4vCPU, 4GB, 30GB disk), large (6vCPU, 8GB, 40GB disk))
During ovf import there should be a settings configurable:
hostname
ip address
netmask
gateway
dns
Opvizor/Cloud API key (register at cloud.opvizor.com to get one)
At first boot the VM settings should be used to configure the VM network automatically
The VM should also either contain telegraf already (or download the binary after first boot and network setup
The telegraf.conf should be changed to use the configured Opvizor/Cloud API key 
```

# Activity summary

### 1. Prepare the VM:
- Installed VMware Workstation 17 on my local PC (I use Linux Fedora 39 with kernel 6.5, [it was need this workaround](https://communities.vmware.com/t5/VMware-Workstation-Pro/Cannot-compile-vmnet-kernel-module-on-kernel-6-4-10/td-p/2982156)).
- Created a VM and installed Debian 12 (from the netinstall ISO).
- Installed various required tools inside the guest VM (telegraf, vmtool, etc.).
- Wrote a bash script to read the env from VMware tools and configure the network, DNS, hostname, and telegraf upon every startup.
- Created a systemd unit to start the script once at boot, before the network and telegraf, but after VMware tools.
- System cleanup, cache clearing, and removal of history.

### 2. Export the VM:
- Exported the VM to OVF.

### 3. Prepare the OVF file:
- Added data in the XML file to prompt during image import (ap-key, ipaddr, netmask, DNS, etc.).


## OVF Tool

```shell
ovftool \
  --X:logToConsole \
  --X:logLevel=verbose \
  --name=largevm \
  --prop:vm.telegraf_output_url="https://sink.opvizor.com" \
  --prop:vm.telegraf_api_key=****** \
  --prop:vm.hostname=largevm \
  --prop:vm.ipaddr=172.16.10.144 \
  --prop:vm.netmask=255.255.255.0 \
  --prop:vm.gateway=172.16.10.1 \
  --prop:vm.dns=1.1.1.1 \
  --deploymentOption=large \
  'vmware-template/Debian 12 Template.ovf' \
  ./vmware-template/

```
### Todo:
- Find a solution to change hardware spec reading the flavor that can be selected on the import phase.
