#!/bin/bash

# Check if dialog is installed
if ! command -v dialog &> /dev/null
then
    echo "Please install 'dialog' to run this script."
    exit 1
fi

source_ovf_path="vmware-template/Debian 12 Template.ovf"
destination_vmx_path="./vmware-template/"


# Function to display flavor options
choose_flavor() {
    dialog --backtitle "VM Configuration" --title "Choose a Flavor" \
           --menu "Select VM Flavor" 15 60 4 \
           1 "Small (2vCPU, 2GB, 20GB disk)" \
           2 "Medium (4vCPU, 4GB, 30GB disk)" \
           3 "Large (6vCPU, 8GB, 40GB disk)" 2>tempfile

    retval=$?
    choice=$(cat tempfile)
    case $retval in
      0)
        case $choice in
        1) flavor="small";;
        2) flavor="medium";;
        3) flavor="large";;
        esac;;
      1)
        echo "Cancel pressed."
        exit 1;;
      255)
        echo "ESC pressed."
        exit 1;;
    esac
}

# Function to get OVF properties
capture_ovf_properties() {
    exec 3>&1
    values=$(dialog --ok-label "Submit" \
      --backtitle "VM Configuration" \
      --title "OVF Properties" \
      --form "Enter the properties" \
      15 60 0 \
      "Telegraf Output URL:" 1 1 "" 1 25 25 0 \
      "Telegraf API Key:"    2 1 "" 2 25 25 0 \
      "Hostname:"            3 1 "" 3 25 25 0 \
      "IP Address:"          4 1 "" 4 25 25 0 \
      "Subnet Mask:"         5 1 "" 5 25 25 0 \
      "Gateway:"             6 1 "" 6 25 25 0 \
      "DNS:"                 7 1 "" 7 25 25 0 \
    2>&1 1>&3)

    exitcode=$?
    exec 3>&-
    case $exitcode in
      0)
        telegraf_output_url=$(echo "$values" | sed -n '1p')
        telegraf_api_key=$(echo "$values" | sed -n '2p')
        hostname=$(echo "$values" | sed -n '3p')
        ipaddr=$(echo "$values" | sed -n '4p')
        netmask=$(echo "$values" | sed -n '5p')
        gateway=$(echo "$values" | sed -n '6p')
        dns=$(echo "$values" | sed -n '7p');;
      1)
        echo "Cancel pressed."
        exit 1;;
      255)
        echo "ESC pressed."
        exit 1;;
    esac
}

choose_flavor
capture_ovf_properties

# Cleanup tempfile
rm -f tempfile

# Display captured details (For verification purposes)
dialog --title "Captured Details" --msgbox "\
Telegraf Output URL: $telegraf_output_url
Telegraf API Key: $telegraf_api_key
Hostname: $hostname
IP Address: $ipaddr
Subnet Mask: $netmask
Gateway: $gateway
DNS: $dns
Flavor: $flavor" 20 60

# Function to import VM using ovftool
import_vm() {

    # Execute ovftool with user's input and VM configurations
    ovftool --name="$hostname" \
            --prop:vm.telegraf_output_url="$telegraf_output_url" \
            --prop:vm.telegraf_api_key="$telegraf_api_key" \
            --prop:vm.hostname="$hostname" \
            --prop:vm.ipaddr="$ipaddr" \
            --prop:vm.netmask="$netmask" \
            --prop:vm.gateway="$gateway" \
            --prop:vm.dns="$dns" \
            --deploymentOption=$flavor \
            "$source_ovf_path" \
            "$destination_vmx_path"

}

import_vm
